<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:svg="http://www.w3.org/2000/svg">

    <xsl:include href="bo_confirmation_format_common.xsl"/>
    <xsl:include href="bo_confirmation_format_pdf_common.xsl"/>
    <xsl:include href="bo_confirmation_format_pdf_CSU_disclaimer.xsl"/>
    <xsl:include href="cs-address-handler.xsl"/>

    <xsl:variable name="footer-height">1in</xsl:variable>
    <xsl:variable name="footer-height-last">0.25in</xsl:variable>
    <xsl:variable name="footer-font-size">8pt</xsl:variable>
    <xsl:variable name="header-height">1.125in</xsl:variable>
    <xsl:variable name="header-height-first-line">1.25in</xsl:variable>
    <xsl:variable name="header-height-second-line">0.75in</xsl:variable>
    <xsl:variable name="header-height-first">2in</xsl:variable>
    <xsl:variable name="header-height-last">0in</xsl:variable>
    <xsl:variable name="region-start-size">1in</xsl:variable>
    <xsl:variable name="region-end-size">0.25in</xsl:variable>
    <xsl:variable name="lhs-col-width">3.75in</xsl:variable>
    <xsl:variable name="rhs-col-width">3.5in</xsl:variable>

    <xsl:variable name="margin-top-last">0.25in</xsl:variable>
    <xsl:variable name="region-start-size-last">0.25in</xsl:variable>
    <xsl:variable name="region-end-size-last">0.25in</xsl:variable>

    <xsl:attribute-set name="page-master-csu">
        <xsl:attribute name="page-height">11in</xsl:attribute>
        <xsl:attribute name="page-width">8.5in</xsl:attribute>
        <xsl:attribute name="margin-top">0.75in</xsl:attribute>
        <xsl:attribute name="margin-bottom">0.25in</xsl:attribute>
        <xsl:attribute name="margin-left">0cm</xsl:attribute>
        <xsl:attribute name="margin-right">0cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="page-master-csu-last" use-attribute-sets="page-master-csu">
        <xsl:attribute name="margin-top">0.25in</xsl:attribute>
        <xsl:attribute name="margin-bottom">0.25in</xsl:attribute>
    </xsl:attribute-set>

    <!-- main reference areas defaults -->
    <xsl:attribute-set name="region-body-csu" use-attribute-sets="region-body">
        <xsl:attribute name="margin-top"><xsl:value-of select="$header-height"/></xsl:attribute>
        <xsl:attribute name="margin-bottom"><xsl:value-of select="$footer-height"/></xsl:attribute>
        <xsl:attribute name="margin-left"><xsl:value-of select="$region-start-size"/></xsl:attribute>
        <xsl:attribute name="margin-right"><xsl:value-of select="$region-end-size"/></xsl:attribute>
        <xsl:attribute name="display-align">center</xsl:attribute>
        <xsl:attribute name="reference-orientation">0</xsl:attribute>
    </xsl:attribute-set>

    <!-- header -->
    <xsl:attribute-set name="region-before-csu" use-attribute-sets="region-before">
        <xsl:attribute name="extent"><xsl:value-of select="$header-height"/></xsl:attribute>
        <xsl:attribute name="display-align">before</xsl:attribute>
    </xsl:attribute-set>

    <!-- footer -->
    <xsl:attribute-set name="region-after-csu" use-attribute-sets="region-after">
        <xsl:attribute name="extent"><xsl:value-of select="$footer-height"/></xsl:attribute>
        <xsl:attribute name="display-align">after</xsl:attribute>
    </xsl:attribute-set>

    <!-- left margin -->
    <xsl:attribute-set name="region-start-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$region-start-size"/></xsl:attribute>
        <xsl:attribute name="precedence">false</xsl:attribute>
    </xsl:attribute-set>

    <!-- right margin -->
    <xsl:attribute-set name="region-end-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$region-end-size"/></xsl:attribute>
        <xsl:attribute name="precedence">true</xsl:attribute>
    </xsl:attribute-set>

    <!-- main reference areas last page modifications -->
    <xsl:attribute-set name="region-body-csu-last" use-attribute-sets="region-body-csu">
        <xsl:attribute name="margin-top"><xsl:value-of select="$header-height-last"/></xsl:attribute>
        <xsl:attribute name="margin-bottom"><xsl:value-of select="$footer-height-last"/></xsl:attribute>
        <xsl:attribute name="margin-left"><xsl:value-of select="$region-start-size-last"/></xsl:attribute>
        <xsl:attribute name="margin-right"><xsl:value-of select="$region-end-size-last"/></xsl:attribute>
        <xsl:attribute name="padding">0pt</xsl:attribute>
        <xsl:attribute name="reference-orientation">90</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-before-csu-last" use-attribute-sets="region-before-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$header-height-last"/></xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-after-csu-last" use-attribute-sets="region-after-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$footer-height-last"/></xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-start-csu-last">
        <xsl:attribute name="extent"><xsl:value-of select="$region-start-size-last"/></xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-end-csu-last">
        <xsl:attribute name="extent"><xsl:value-of select="$region-end-size-last"/></xsl:attribute>
    </xsl:attribute-set>


    <xsl:template match="/">
        <fo:root font-family="Credit Suisse Type Light">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="pageFirst" xsl:use-attribute-sets="page-master-csu">
                    <!-- make header taller on first page -->
                    <fo:region-body   xsl:use-attribute-sets="region-body-csu"   margin-top="{$header-height-first}"/>
                    <fo:region-before xsl:use-attribute-sets="region-before-csu" extent="{$header-height-first}" region-name="header-first-page"/>
                    <fo:region-after  xsl:use-attribute-sets="region-after-csu"  region-name="master-footer-odd"/>
                    <fo:region-start  xsl:use-attribute-sets="region-start-csu"  region-name="region-start-first" reference-orientation="90"/>
                    <fo:region-end    xsl:use-attribute-sets="region-end-csu"/>
                </fo:simple-page-master>

                <fo:simple-page-master master-name="pageOdd" xsl:use-attribute-sets="page-master-csu">
                    <fo:region-body   xsl:use-attribute-sets="region-body-csu"/>
                    <fo:region-before xsl:use-attribute-sets="region-before-csu" region-name="header"/>
                    <fo:region-after  xsl:use-attribute-sets="region-after-csu"  region-name="master-footer-odd"/>
                    <fo:region-start  xsl:use-attribute-sets="region-start-csu"/>
                    <fo:region-end    xsl:use-attribute-sets="region-end-csu"/>
                </fo:simple-page-master>

                <fo:simple-page-master master-name="pageOddLast" xsl:use-attribute-sets="page-master-csu-last">
                    <fo:region-body   xsl:use-attribute-sets="region-body-csu-last" />
                    <fo:region-before xsl:use-attribute-sets="region-before-csu-last"/>
                    <fo:region-after  xsl:use-attribute-sets="region-after-csu-last"  region-name="master-footer-odd-last"/>
                    <fo:region-start  xsl:use-attribute-sets="region-start-csu-last"/>
                    <fo:region-end    xsl:use-attribute-sets="region-end-csu-last"/>
                </fo:simple-page-master>

                <fo:simple-page-master master-name="pageEven" xsl:use-attribute-sets="page-master-csu">
                    <fo:region-body   xsl:use-attribute-sets="region-body-csu"/>
                    <fo:region-before xsl:use-attribute-sets="region-before-csu" region-name="header"/>
                    <fo:region-after  xsl:use-attribute-sets="region-after-csu"  region-name="master-footer-even"/>
                    <fo:region-start  xsl:use-attribute-sets="region-start-csu" />
                    <fo:region-end    xsl:use-attribute-sets="region-end-csu"/>
                </fo:simple-page-master>

                <fo:simple-page-master master-name="pageEvenLast" xsl:use-attribute-sets="page-master-csu-last">
                    <fo:region-body   xsl:use-attribute-sets="region-body-csu-last" />
                    <fo:region-before xsl:use-attribute-sets="region-before-csu-last"/>
                    <fo:region-after  xsl:use-attribute-sets="region-after-csu-last"  region-name="master-footer-even-last"/>
                    <fo:region-start  xsl:use-attribute-sets="region-start-csu-last"/>
                    <fo:region-end    xsl:use-attribute-sets="region-end-csu-last"/>
                </fo:simple-page-master>

                <fo:page-sequence-master master-name="Layout">
                    <fo:single-page-master-reference master-reference="pageFirst"/>
                    <fo:repeatable-page-master-alternatives>
                        <fo:conditional-page-master-reference master-reference="pageOdd"      odd-or-even="odd"/>
                        <fo:conditional-page-master-reference master-reference="pageEven"     odd-or-even="even"/>
                    </fo:repeatable-page-master-alternatives>
                </fo:page-sequence-master>

                <fo:page-sequence-master master-name="Disclaimer">
                    <fo:repeatable-page-master-alternatives>
                        <fo:conditional-page-master-reference master-reference="pageOddLast"  odd-or-even="odd"/>
                        <fo:conditional-page-master-reference master-reference="pageEvenLast" odd-or-even="even"/>
                    </fo:repeatable-page-master-alternatives>
                </fo:page-sequence-master>

            </fo:layout-master-set>

            <xsl:apply-templates select="//CONFIRMATION_CANONICAL"/>

        </fo:root>
</xsl:template>

        <xsl:template match="CONFIRMATION_CANONICAL">
            <fo:page-sequence master-reference="Layout" initial-page-number="1">
                <fo:static-content flow-name="header">
                    <xsl:call-template name="header-csu">
                        <xsl:with-param name="show-contact-info">false</xsl:with-param>
                        <xsl:with-param name="generated-date"><xsl:value-of select="HEADER/GENERATED_DATE"/></xsl:with-param>
                        <xsl:with-param name="account-number"><xsl:value-of select="HEADER/ACCOUNT_NUMBER"/></xsl:with-param>
                    </xsl:call-template>
                </fo:static-content>

                <fo:static-content flow-name="header-first-page">
                    <xsl:call-template name="header-csu">
                        <xsl:with-param name="show-contact-info">true</xsl:with-param>
                        <xsl:with-param name="generated-date"><xsl:value-of select="HEADER/GENERATED_DATE"/></xsl:with-param>
                        <xsl:with-param name="account-number"><xsl:value-of select="HEADER/ACCOUNT_NUMBER"/></xsl:with-param>
                    </xsl:call-template>
                </fo:static-content>

                <fo:static-content flow-name="region-start-first">
                    <fo:block font-size="6pt" text-align="center" padding-top="0.25in">
                        <xsl:value-of select="HEADER/GENERATED_DATE_LC"/>
                    </fo:block>
                </fo:static-content>

                <fo:static-content flow-name="master-footer-even">
                    <fo:block block-progression-dimension="{$footer-height}" font-size="{$footer-font-size}">
                        <fo:block text-align="left">Page <fo:page-number/></fo:block>
                    </fo:block>
                </fo:static-content>

                <fo:static-content flow-name="master-footer-odd">
                    <fo:block block-progression-dimension="{$footer-height}" block-progression-dimension.maximum="{$footer-height}" font-size="{$footer-font-size}">
                        <fo:block text-align="right" block-progression-dimension.minimum="0.55in" block-progression-dimension.maximum="0.7in" block-progression-dimension.optimum="0.6in">
                                <fo:external-graphic src="url(images/cipf.png)" height="0.33in" content-height="scale-down-to-fit" padding="0.11in 0in" scaling="uniform"/>
                        </fo:block>
                        <fo:block text-align="right">Page <fo:page-number/></fo:block>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">

                    <fo:block-container space-after="1cm" top="-0.3in" font-size="9pt" position="absolute">
                        <fo:block inline-progression-dimension="4in">
                            <xsl:choose> 
                                <xsl:when test="POSTAL_ADDRESS/POSTAL_ADDRESS_DATA">
                                    <xsl:message>Using new postal address XML template</xsl:message>
                                    <xsl:apply-templates select="POSTAL_ADDRESS/POSTAL_ADDRESS_DATA"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:message>Using old postal address XML template</xsl:message>
                                    <xsl:call-template name="replace">
                                            <xsl:with-param name="string"><xsl:value-of select="POSTAL_ADDRESS"/></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </fo:block>
                    </fo:block-container>

                    <!-- main content area -->
                    <xsl:apply-templates select="CONFIRMATION"/>

                </fo:flow>
            </fo:page-sequence> <!-- main area without the disclaimer -->

            <fo:page-sequence master-reference="Disclaimer">
                <!-- disclaimer on last page -->
                <fo:static-content flow-name="master-footer-odd-last">
                    <fo:block block-progression-dimension="{$footer-height-last}" font-size="{$footer-font-size}">
                        <fo:block text-align="right">Page <fo:page-number/></fo:block>
                    </fo:block>
                </fo:static-content>

                <fo:static-content flow-name="master-footer-even-last">
                    <fo:block block-progression-dimension="{$footer-height-last}" font-size="{$footer-font-size}">
                        <fo:block text-align="left">Page <fo:page-number/></fo:block>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container page-break-before="always">
                        <xsl:call-template name="csu-disclaimer"/>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence> <!-- disclaimer -->
    </xsl:template>

    <xsl:template match="NUMBER">
        <xsl:variable name="numberFormat">
            <xsl:choose>
                <xsl:when test="@TYPE = 'INTEGER'">#,##0</xsl:when>
                <xsl:when test="@TYPE = 'PRICE'">#,##0.0000</xsl:when>
                <xsl:when test="@TYPE = 'SHORT_PRICE'">#,##0.00</xsl:when>
                <xsl:otherwise>#,##0.0000</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="@TYPE = 'PRICE'">
                <!-- for prices we've been asked to truncate the number, not round it -->
                <xsl:value-of select="format-number(floor(. * 10000) div 10000, $numberFormat)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="format-number(., $numberFormat)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="replace">
        <xsl:param name="string"/>
        <xsl:choose>
            <xsl:when test="contains($string,'&#44;')">
                <fo:block>
                    <xsl:value-of select="substring-before($string,'&#44;')"/>
                </fo:block>
                <xsl:call-template name="replace">
                    <xsl:with-param name="string" select="substring-after($string,'&#44;')"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <fo:block>
                    <xsl:value-of select="$string"/>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="header-csu">
        <xsl:param name="show-contact-info"/>
        <xsl:param name="generated-date"/>
        <xsl:param name="account-number"/>
        <fo:block font-weight="bold" font-size="8pt" >
            <fo:table table-layout="fixed" width="7.25in">
                <fo:table-column column-width="{$lhs-col-width}"/>
                <fo:table-column column-width="{$rhs-col-width}"/>
                <fo:table-body>
                    <fo:table-row height="{$header-height-first-line}">
                        <fo:table-cell display-align="after">
                            <fo:block block-progression-dimension="{$header-height-first-line}"> <!-- first line of address -->
                                <fo:block block-progression-dimension="0.75in"> <!-- image block -->
                                    <fo:external-graphic src="url(images/credit_suisse_sp_rgb_fo_100mm.svg)" content-height="0.75in" overflow="visible"/>
                                </fo:block> <!-- image block -->
                                <fo:block absolute-position="auto" font-style="italic" font-weight="normal" display-align="after"> <!-- address block -->
                                    <xsl:choose>
                                        <xsl:when test="$show-contact-info = 'true'">
                                            <fo:block>1 FIRST CANADIAN PLACE</fo:block>
                                            <fo:block>29TH FLOOR</fo:block>
                                            <fo:block>TORONTO ONTARIO M5X 1C9</fo:block>
                                        </xsl:when>
                                        <xsl:otherwise><fo:block><xsl:text> </xsl:text></fo:block></xsl:otherwise>
                                    </xsl:choose>
                                </fo:block> <!-- address block -->
                            </fo:block> <!-- first line of address -->
                        </fo:table-cell>
                        <fo:table-cell display-align="after">
                            <fo:block margin-bottom="0.375in" font-size="12pt" text-align="right">CONFIRMATION NOTICE</fo:block>
                            <fo:block>
                                <fo:table width="100%" table-layout="fixed">
                                    <fo:table-column column-width="2in"/>
                                    <fo:table-column column-width="1.5in"/>

                                    <fo:table-header>
                                        <fo:table-row>
                                            <fo:table-cell><fo:block>DATE</fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>ACCOUNT NUMBER</fo:block></fo:table-cell>
                                        </fo:table-row>
                                    </fo:table-header>

                                    <fo:table-body>
                                        <fo:table-row>
                                            <fo:table-cell><fo:block font-weight="normal"><xsl:value-of select="$generated-date"/></fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block><xsl:value-of select="$account-number"/></fo:block></fo:table-cell>
                                        </fo:table-row>
                                    </fo:table-body>

                                </fo:table>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <xsl:if test="$show-contact-info = 'true'">
                        <fo:table-row height="{$header-height-second-line}">
                            <fo:table-cell><fo:block/></fo:table-cell>
                            <fo:table-cell display-align="after">
                                <fo:block>
                                    <fo:block>MEMBERSHIP OR OFFICE INFORMATION</fo:block>
                                    <fo:block font-weight="normal">
                                        <fo:block>Head Office Phone: 416-352-4500</fo:block>
                                        <fo:block>Head Office Fax: 416-352-4689</fo:block>
                                        <fo:block>Web: www.credit-suisse.com</fo:block>
                                    </fo:block>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <!--
        If a confirmation-specific EOD_NAC_FLAG exists, then choose it over the 
        global "$nacflag" parameter passed via the trasformation environment
        -->

    <xsl:template name="confirmation-nacFlag">
        <xsl:choose>
            <xsl:when test="../EOD_NAC_FLAG"><xsl:value-of select="../EOD_NAC_FLAG"/></xsl:when>
            <xsl:when test="./EOD_NAC_FLAG"><xsl:value-of select="./EOD_NAC_FLAG"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$nacflag"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

<xsl:template name="confirmation-single" match="CONFIRMATION">
    <fo:block text-align="start" font-size="8pt" line-height="10pt" border-bottom="1pt solid black" border-collapse="collapse" padding-top="5pt" keep-together="always" border-top="1pt solid black">
        <xsl:apply-templates/>
    </fo:block>
</xsl:template>

<xsl:template name="narrative" match="NARRATIVE">
    <fo:block space-start="0.125pt" padding-left="15pt" padding-right="10pt">
        <xsl:value-of select="."/>
    </fo:block>
</xsl:template>

<xsl:template name="transaction-summary" match="TRANSACTION_SUMMARY">

    <xsl:variable name="nacFlagFinal">
        <xsl:call-template name="confirmation-nacFlag"/>
    </xsl:variable>

    <xsl:variable name="transactionTypeLabel">
        <xsl:choose>
            <xsl:when test="$nacFlagFinal = 'C'">CANCELLED</xsl:when>
            <xsl:otherwise><xsl:value-of select="TRANSACTION_TYPE"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <fo:block space-after="5pt"> <!-- grey area -->
        <fo:table table-layout="fixed" background-color="rgb(189,189,189)" border="0.5pt solid black" border-collapse="collapse" width="100%">
            <fo:table-column column-width="1.5in"/>
            <fo:table-column column-width="2.25in"/>
            <fo:table-column column-width="0.375in"/>
            <fo:table-column column-width="3.125in"/>

            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell><fo:block font-weight="bold" font-size="10pt" line-height="12pt">TRANSACTION TYPE:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-weight="bold" font-size="10pt" line-height="12pt"><xsl:value-of select="$transactionTypeLabel"/></fo:block></fo:table-cell>
                    <fo:table-cell number-columns-spanned="2"><fo:block>REGISTERED REPRESENTATIVE</fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block>Settlement Date:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="SETTLEMENT_DATE"/></fo:block></fo:table-cell>
                    <fo:table-cell><fo:block>Name:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="REGISTERED_REPRESENTATIVE/REP_NAME"/></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block>Account:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="ACCOUNT"/></fo:block></fo:table-cell>
                    <fo:table-cell><fo:block>Code:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="REGISTERED_REPRESENTATIVE/REP_CODE"/></fo:block></fo:table-cell>
                </fo:table-row>
            </fo:table-body>

        </fo:table>
    </fo:block> <!-- grey area -->
</xsl:template>

<xsl:template name="transaction-details" match="TRANSACTION_DETAILS">
    <fo:block space-before="2pt" font-size="8pt" line-height="10pt" margin-bottom="0.5in"> <!-- security details and charges area -->
        <fo:table table-layout="fixed" width="7.25in">
            <fo:table-column column-width="3.625in"/>
            <fo:table-column column-width="0.25in"/>
            <fo:table-column column-width="3.375in"/>

            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block>
                            <xsl:apply-templates select="SECURITY_TRADED"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block/>
                    </fo:table-cell>
                    <fo:table-cell display-align="after" padding-left="10pt" padding-right="10pt">
                        <fo:block>
                            <xsl:apply-templates select="TRANSACTION_CHARGES"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </fo:block> <!-- security details and charges area -->
</xsl:template>

<xsl:template name="security-details" match="SECURITY_TRADED">
    <fo:block>
        <fo:table table-layout="fixed" width="100%"> <!-- security details -->
            <fo:table-column column-width="0.625in"/>
            <fo:table-column column-width="proportional-column-width(1)"/>
            <fo:table-column column-width="0.825in"/>

            <fo:table-header>
                <fo:table-row border-bottom="solid thin black">
                    <fo:table-cell><fo:block>Quantity</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block>Security Description</fo:block></fo:table-cell>
                    <fo:table-cell text-align="right"><fo:block>Unit Price</fo:block></fo:table-cell>
                </fo:table-row>
            </fo:table-header>

            <fo:table-body>
                <fo:table-row font-weight="bold">
                    <fo:table-cell><fo:block><xsl:apply-templates select="SEC_QUANTITY"/></fo:block></fo:table-cell>
                    <fo:table-cell><fo:block wrap-option="wrap" keep-together="auto"><xsl:value-of select="SEC_NAME_FULL"/></fo:block></fo:table-cell>
                    <fo:table-cell text-align="right"><fo:block><xsl:apply-templates select="SEC_UNIT_PRICE"/><xsl:value-of select="SEC_UNIT_PRICE_CCY"/></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block></fo:block></fo:table-cell>
                    <fo:table-cell number-columns-spanned="2"><fo:block><xsl:apply-templates select="SEC_DESCRIPTION"/></fo:block></fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table> <!-- security details -->
    </fo:block>
</xsl:template>

<xsl:template name="transaction-charges" match="TRANSACTION_CHARGES">
    <fo:block border="thin solid black" background-color="cyan" inline-progression-dimension="100%" margin-top="10pt" text-align="center" padding-left="10pt" padding-right="10pt">
        <fo:table table-layout="fixed" width="100%" text-align="left"> <!-- cyan table with charges -->
            <fo:table-column column-width="1.5in"/>
            <fo:table-column column-width="proportional-column-width(1)" text-align="right"/>
            <fo:table-column column-width="0.375in" text-align="right"/>

            <fo:table-footer>
                <fo:table-row font-weight="bold" border-top="thin solid black">
                    <fo:table-cell><fo:block padding-top="0.0625in">SUB TOTAL</fo:block></fo:table-cell>
                    <fo:table-cell text-align="right"><fo:block><xsl:apply-templates select="SUB_TOTAL"/></fo:block></fo:table-cell>
                    <fo:table-cell text-align="right"><fo:block><xsl:value-of select="SUB_TOTAL_CCY"/></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <xsl:call-template name="charges-table-row">
                        <xsl:with-param name="rowDisplayedName">EXCHANGE</xsl:with-param>
                        <xsl:with-param name="namedValueElement" select="EXCHANGE"/>
                    </xsl:call-template>
                </fo:table-row>
                <fo:table-row font-weight="bold" border-top="thin solid black">
                    <fo:table-cell><fo:block padding-top="0.0625in">NET AMOUNT</fo:block></fo:table-cell>
                    <fo:table-cell text-align="right"><fo:block><xsl:apply-templates select="NET_AMOUNT"/></fo:block></fo:table-cell>
                    <fo:table-cell text-align="right"><fo:block><xsl:value-of select="NET_AMOUNT_CCY"/></fo:block></fo:table-cell>
                </fo:table-row>
            </fo:table-footer>

            <fo:table-body>
                <fo:table-row>
                    <xsl:call-template name="charges-table-row">
                        <xsl:with-param name="rowDisplayedName">GROSS AMOUNT</xsl:with-param>
                        <xsl:with-param name="namedValueElement" select="GROSS_AMOUNT"/>
                    </xsl:call-template>
                </fo:table-row>
                <fo:table-row>
                    <xsl:call-template name="charges-table-row">
                        <xsl:with-param name="rowDisplayedName">COMMISSION</xsl:with-param>
                        <xsl:with-param name="namedValueElement" select="COMMISSION"/>
                    </xsl:call-template>
                </fo:table-row>
                <fo:table-row>
                    <xsl:call-template name="charges-table-row">
                        <xsl:with-param name="rowDisplayedName">INTEREST</xsl:with-param>
                        <xsl:with-param name="namedValueElement" select="INTEREST"/>
                    </xsl:call-template>
                </fo:table-row>
                <fo:table-row>
                    <xsl:call-template name="charges-table-row">
                        <xsl:with-param name="rowDisplayedName">SEC FEE</xsl:with-param>
                        <xsl:with-param name="namedValueElement" select="SEC_FEE"/>
                    </xsl:call-template>
                </fo:table-row>
                <fo:table-row>
                    <xsl:call-template name="charges-table-row">
                        <xsl:with-param name="rowDisplayedName">POSTAGE</xsl:with-param>
                        <xsl:with-param name="namedValueElement" select="POSTAGE"/>
                    </xsl:call-template>
                </fo:table-row>
                <fo:table-row>
                    <xsl:call-template name="charges-table-row">
                        <xsl:with-param name="rowDisplayedName">GOVERNMENT FEE</xsl:with-param>
                        <xsl:with-param name="namedValueElement" select="GOVERNMENT_FEE"/>
                    </xsl:call-template>
                </fo:table-row>
            </fo:table-body>
        </fo:table> <!-- cyan table with charges -->
    </fo:block>
</xsl:template>

<xsl:template name="charges-table-row">
    <xsl:param name="rowDisplayedName"/>
    <xsl:param name="namedValueElement"/>
    <xsl:choose>
        <xsl:when test="$namedValueElement">
            <fo:table-cell><fo:block><xsl:value-of select="$rowDisplayedName"/></fo:block></fo:table-cell>
            <fo:table-cell text-align="right"><fo:block><xsl:apply-templates select="$namedValueElement"/></fo:block></fo:table-cell>
            <fo:table-cell text-align="right"><fo:block></fo:block></fo:table-cell>
        </xsl:when>
        <xsl:otherwise>
            <fo:table-cell><fo:block white-space-treatment="preserve"><xsl:text> </xsl:text></fo:block></fo:table-cell>
            <fo:table-cell text-align="right"><fo:block></fo:block></fo:table-cell>
            <fo:table-cell text-align="right"><fo:block></fo:block></fo:table-cell>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="reference-data" match="REFERENCE_DATA">
    <fo:block font-size="6pt"> <!-- section footer -->
        <xsl:if test="OUR_REF">
            <fo:inline>Reference </fo:inline>
            <fo:inline><xsl:value-of select="OUR_REF"/></fo:inline>
            <fo:inline>|</fo:inline>
        </xsl:if>
        <xsl:if test="ISIN_CUSIP">
            <fo:inline>ISIN/CUSIP</fo:inline>
            <fo:inline><xsl:value-of select="ISIN_CUSIP"/></fo:inline>
            <fo:inline>|</fo:inline>
        </xsl:if>
        <xsl:if test="TICKER_SYMBOL">
            <fo:inline>Ticker</fo:inline>
            <fo:inline><xsl:value-of select="TICKER_SYMBOL"/></fo:inline>
            <fo:inline>|</fo:inline>
        </xsl:if>
        <xsl:if test="ACCOUNT_NUM_ABBR">
            <fo:inline><xsl:value-of select="ACCOUNT_NUM_ABBR"/></fo:inline>
            <fo:inline>|</fo:inline>
        </xsl:if>
        <xsl:if test="SUPPRESSION_FLAG">
            <fo:inline><xsl:value-of select="SUPPRESSION_FLAG"/></fo:inline>
        </xsl:if>
    </fo:block> <!-- section footer -->
</xsl:template>

<xsl:template name="ignore-nac-flag" match="EOD_NAC_FLAG">
</xsl:template>

<xsl:template match="SEC_DESCRIPTION">
    <xsl:for-each select="SEC_DESC_LINE">
        <fo:block><xsl:value-of select="."/></fo:block>
    </xsl:for-each>
</xsl:template>

<xsl:template match="NARRATIVE">
    <xsl:variable name="buySell">
        <xsl:choose>
            <xsl:when test="IS_BUY = 'true'">B</xsl:when>
            <xsl:when test="IS_BUY = 'false'">S</xsl:when>
            <xsl:otherwise><xsl:message terminate="yes">Invalid buy/sell indicator</xsl:message></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="nacFlagFinal">
        <xsl:call-template name="confirmation-nacFlag"/>
    </xsl:variable>

    <xsl:variable name="narrative-picker"><xsl:value-of select="BLOTTER_CODE"/>-<xsl:if test="$nacFlagFinal = 'C'">X</xsl:if><xsl:value-of select="$buySell"/></xsl:variable>

    <fo:block font-size="8pt" wrap-option="wrap" keep-together="auto">
        <xsl:choose>
            <xsl:when test="$narrative-picker = 'AD-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON ALPHA</xsl:when>
            <xsl:when test="$narrative-picker = 'AD-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON ALPHA</xsl:when>
            <xsl:when test="$narrative-picker = 'AD-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON ALPHA</xsl:when>
            <xsl:when test="$narrative-picker = 'AD-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON ALPHA</xsl:when>
            <xsl:when test="$narrative-picker = 'AF-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON TMX SELECT</xsl:when>
            <xsl:when test="$narrative-picker = 'AF-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON TMX SELECT</xsl:when>
            <xsl:when test="$narrative-picker = 'AF-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON TMX SELECT</xsl:when>
            <xsl:when test="$narrative-picker = 'AF-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON TMX SELECT</xsl:when>
            <xsl:when test="$narrative-picker = 'AG-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON OMEGA</xsl:when>
            <xsl:when test="$narrative-picker = 'AG-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON OMEGA</xsl:when>
            <xsl:when test="$narrative-picker = 'AG-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON OMEGA</xsl:when>
            <xsl:when test="$narrative-picker = 'AG-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON OMEGA</xsl:when>
            <xsl:when test="$narrative-picker = 'AJ-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON PURE TRADING.</xsl:when>
            <xsl:when test="$narrative-picker = 'AJ-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON PURE TRADING.</xsl:when>
            <xsl:when test="$narrative-picker = 'AJ-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON PURE TRADING.</xsl:when>
            <xsl:when test="$narrative-picker = 'AJ-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON PURE TRADING.</xsl:when>
            <xsl:when test="$narrative-picker = 'AK-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON MATCH NOW.</xsl:when>
            <xsl:when test="$narrative-picker = 'AK-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON MATCH NOW.</xsl:when>
            <xsl:when test="$narrative-picker = 'AK-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON MATCH NOW.</xsl:when>
            <xsl:when test="$narrative-picker = 'AK-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON MATCH NOW.</xsl:when>
            <xsl:when test="$narrative-picker = 'AL-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON CHIX.</xsl:when>
            <xsl:when test="$narrative-picker = 'AL-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON CHIX.</xsl:when>
            <xsl:when test="$narrative-picker = 'AL-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON CHIX.</xsl:when>
            <xsl:when test="$narrative-picker = 'AL-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON CHIX.</xsl:when>
            <xsl:when test="$narrative-picker = 'AV-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON THE TSX VENTURE EXCHANGE.</xsl:when>
            <xsl:when test="$narrative-picker = 'AV-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON THE TSX VENTURE EXCHANGE.</xsl:when>
            <xsl:when test="$narrative-picker = 'AV-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON THE TSX VENTURE EXCHANGE.</xsl:when>
            <xsl:when test="$narrative-picker = 'AV-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON THE TSX VENTURE EXCHANGE.</xsl:when>
            <xsl:when test="$narrative-picker = 'AZ-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON THE TORONTO STOCK EXCHANGE.</xsl:when>
            <xsl:when test="$narrative-picker = 'AZ-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON THE TORONTO STOCK EXCHANGE.</xsl:when>
            <xsl:when test="$narrative-picker = 'AZ-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON THE TORONTO STOCK EXCHANGE.</xsl:when>
            <xsl:when test="$narrative-picker = 'AZ-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON THE TORONTO STOCK EXCHANGE.</xsl:when>
            <xsl:when test="$narrative-picker = 'TL-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON MULTIPLE MARKETPLACES. DETAILS ARE AVAILABLE UPON REQUEST.</xsl:when>
            <xsl:when test="$narrative-picker = 'TL-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON MULTIPLE MARKETPLACES. DETAILS ARE AVAILABLE UPON REQUEST.</xsl:when>
            <xsl:when test="$narrative-picker = 'TL-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON MULTIPLE MARKETPLACES. DETAILS ARE AVAILABLE UPON REQUEST.</xsl:when>
            <xsl:when test="$narrative-picker = 'TL-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON MULTIPLE MARKETPLACES. DETAILS ARE AVAILABLE UPON REQUEST.</xsl:when>
            <xsl:when test="$narrative-picker = '64-B'">TODAY WE CONFIRM THE FOLLOWING BUY FOR YOUR ACCOUNT ON MULTIPLE MARKETPLACES. DETAILS ARE AVAILABLE UPON REQUEST.</xsl:when>
            <xsl:when test="$narrative-picker = '64-S'">TODAY WE CONFIRM THE FOLLOWING SALE FOR YOUR ACCOUNT ON MULTIPLE MARKETPLACES. DETAILS ARE AVAILABLE UPON REQUEST.</xsl:when>
            <xsl:when test="$narrative-picker = '64-XB'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING PURCHASE FOR YOUR ACCOUNT ON MULTIPLE MARKETPLACES. DETAILS ARE AVAILABLE UPON REQUEST.</xsl:when>
            <xsl:when test="$narrative-picker = '64-XS'">TODAY WE CONFIRM THE CANCELLATION OF THE FOLLOWING SALE FOR YOUR ACCOUNT ON MULTIPLE MARKETPLACES. DETAILS ARE AVAILABLE UPON REQUEST.</xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">Invalid NAC flag or blotter code for confirmation narrative generation. Check your configuration</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </fo:block>
</xsl:template>

</xsl:stylesheet>
