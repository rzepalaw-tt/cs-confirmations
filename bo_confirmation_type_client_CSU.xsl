<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
                xmlns:math='http://exslt.org/math' extension-element-prefixes="math">
    <!-- Generate canonical confirmation XML for a CLIENT Confirmation -->
    <xsl:output method="xml" indent="yes" doctype-system="confirmation_canonical_CSU.dtd" encoding="UTF-8"/>

    <xsl:include href="bo_confirmation_type_common.xsl"/>
    <xsl:include href="cs-trailer-codes_CSU.xsl"/>

    <xsl:template match="ENVIRONMENT">

        <xsl:variable name="quantityStripped">
            <xsl:value-of select="format-number(math:abs(QUANTITY), '#,##0.####')"/>
        </xsl:variable>

        <xsl:variable name="negate">
            <xsl:choose>
                <xsl:when test="QUANTITY &gt; 0"><xsl:value-of select="number(1)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="number(-1)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="boughtSold">
            <xsl:choose>
                <xsl:when test="IS_CLIENT_TRADE = 'TRUE'">
                    <xsl:choose>
                        <xsl:when test="QUANTITY &gt; 0">BOUGHT</xsl:when>
                        <xsl:otherwise>SOLD</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="QUANTITY &gt; 0">SOLD</xsl:when>
                        <xsl:otherwise>BOUGHT</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="transactionTypeLabel">
            <xsl:choose>
                <xsl:when test="$boughtSold = 'BOUGHT'">BUY</xsl:when>
                <xsl:otherwise>SOLD</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- only add the header once. This checks if we already processed an ENVIRONMENT node (i.e. if there exists a previous node called ENVIRONMENT).
             If we did, do not add a header -->
        <xsl:if test="not(preceding-sibling::ENVIRONMENT)">
            <HEADER>
                <GENERATED_DATE><xsl:apply-templates select="../CONFIRMATION_GENERATED_DATE"/></GENERATED_DATE>
                <GENERATED_DATE_LC>
                  <xsl:apply-templates select="../CONFIRMATION_GENERATED_DATE/DATE_TIME">
                    <xsl:with-param name="allCaps">false</xsl:with-param>
                  </xsl:apply-templates>
                </GENERATED_DATE_LC>
                <ACCOUNT_NUMBER>
                    <xsl:value-of select="concat(substring(ACCOUNT_NAME,1,3),
                                                 '-',
                                                 substring(ACCOUNT_NAME,4,5))"/>
                </ACCOUNT_NUMBER>
            </HEADER>

            <xsl:choose>
                <xsl:when test="EOD_POSTAL_ADDRESS">
                    <POSTAL_ADDRESS><xsl:value-of select="EOD_POSTAL_ADDRESS"/></POSTAL_ADDRESS>
                </xsl:when>
                <xsl:otherwise>
                    <POSTAL_ADDRESS>%%POSTAL_ADDRESS%%</POSTAL_ADDRESS>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>

        <CONFIRMATION>

            <NARRATIVE>
                <IS_BUY><xsl:value-of select="$boughtSold = 'BOUGHT'"/></IS_BUY>
                <BLOTTER_CODE><xsl:value-of select="BLOTTER_CODE"/></BLOTTER_CODE>
            </NARRATIVE>

            <TRANSACTION_SUMMARY>
                <!-- note the TRANSACTION_TYPE can be overrriden later if the trade is a cancel. This is done in supervisor -->
                <TRANSACTION_TYPE><xsl:value-of select="$transactionTypeLabel"/></TRANSACTION_TYPE>
                <SETTLEMENT_DATE><xsl:apply-templates select="VALUE_DATE"/></SETTLEMENT_DATE>
                <ACCOUNT>
                    <xsl:choose>
                        <xsl:when test="substring(ACCOUNT_NAME,1,1) = '2'">DAP</xsl:when>
                        <xsl:when test="substring(ACCOUNT_NAME,1,1) = '9'">FTR</xsl:when>
                        <xsl:otherwise>
                            <xsl:message terminate="yes">Invalid account type for account number "<xsl:value-of select="ACCOUNT_NAME"/>"</xsl:message>
                        </xsl:otherwise>
                    </xsl:choose>
                </ACCOUNT>
                <REGISTERED_REPRESENTATIVE>
                    <REP_NAME><xsl:value-of select="REGISTERED_REPRESENTATIVE"/></REP_NAME>
                    <REP_CODE><xsl:value-of select="RR_CODE"/></REP_CODE>
                </REGISTERED_REPRESENTATIVE>
            </TRANSACTION_SUMMARY>

            <TRANSACTION_DETAILS>
                <SECURITY_TRADED>
                    <SEC_QUANTITY><NUMBER TYPE="INTEGER"><xsl:value-of select="QUANTITY * $negate"/></NUMBER></SEC_QUANTITY>
                    <SEC_NAME_FULL><xsl:value-of select="SECURITY_NAME"/></SEC_NAME_FULL>
                    <SEC_UNIT_PRICE><NUMBER TYPE="PRICE"><xsl:value-of select="GROSS_PRICE"/></NUMBER></SEC_UNIT_PRICE>
                    <SEC_UNIT_PRICE_CCY><xsl:value-of select="GROSS_PRICE_CURRENCY"/></SEC_UNIT_PRICE_CCY>
                    <SEC_DESCRIPTION>
                        <xsl:if test="TRADING_CAPACITY">
                            <xsl:choose>
                                <xsl:when test="TRADING_CAPACITY = 'P'">
                                    <SEC_DESC_LINE>
                                        <xsl:call-template name="trailer-text">
                                            <xsl:with-param name="trailer-code">
                                                <xsl:value-of select="$trailer-principal"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </SEC_DESC_LINE>
                                </xsl:when>
                                <xsl:when test="TRADING_CAPACITY = 'A'">
                                    <SEC_DESC_LINE>
                                        <xsl:call-template name="trailer-text">
                                            <xsl:with-param name="trailer-code">
                                                <xsl:value-of select="$trailer-agent"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </SEC_DESC_LINE>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:message>Invalid trading capacity trailer</xsl:message>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>

                        <xsl:if test="NCIB_INDICATOR = 'T'">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="trailer-text">
                                    <xsl:with-param name="trailer-code">
                                        <xsl:value-of select="$trailer-ncib"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>

                        <xsl:if test="IS_SHORT_SELL = 'T'">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="trailer-text">
                                    <xsl:with-param name="trailer-code">
                                        <xsl:value-of select="$trailer-short"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>

                        <xsl:if test="SOLICITED_INDICATOR">
                            <xsl:choose>
                                <xsl:when test="SOLICITED_INDICATOR = 'T'">
                                    <SEC_DESC_LINE>
                                        <xsl:call-template name="trailer-text">
                                            <xsl:with-param name="trailer-code">
                                                <xsl:value-of select="$trailer-solicited"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </SEC_DESC_LINE>
                                </xsl:when>
                                <xsl:otherwise>
                                    <SEC_DESC_LINE>
                                        <xsl:call-template name="trailer-text">
                                            <xsl:with-param name="trailer-code">
                                                <xsl:value-of select="$trailer-unsolicited"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </SEC_DESC_LINE>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>

                        <xsl:if test="IF_AS_WHEN_INDICATOR = 'T'">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="trailer-text">
                                    <xsl:with-param name="trailer-code">
                                        <xsl:value-of select="$trailer-ifaswhen"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>

                        <xsl:if test="MEMO1">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="massage-memo-text">
                                    <xsl:with-param name="text" select="MEMO1"/> 
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>
                        <xsl:if test="MEMO2">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="massage-memo-text">
                                    <xsl:with-param name="text" select="MEMO2"/> 
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>
                        <xsl:if test="MEMO3">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="massage-memo-text">
                                    <xsl:with-param name="text" select="MEMO3"/> 
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>
                        <xsl:if test="MEMO4">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="massage-memo-text">
                                    <xsl:with-param name="text" select="MEMO4"/> 
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>
                        <xsl:if test="MEMO5">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="massage-memo-text">
                                    <xsl:with-param name="text" select="MEMO5"/> 
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>
                        <xsl:if test="MEMO6">
                            <SEC_DESC_LINE>
                                <xsl:call-template name="massage-memo-text">
                                    <xsl:with-param name="text" select="MEMO6"/> 
                                </xsl:call-template>
                            </SEC_DESC_LINE>
                        </xsl:if>
                    </SEC_DESCRIPTION>
                </SECURITY_TRADED>
                <TRANSACTION_CHARGES>
                    <GROSS_AMOUNT><NUMBER TYPE="SHORT_PRICE"><xsl:value-of select="GROSS_AMOUNT * $negate"/></NUMBER></GROSS_AMOUNT>

                    <xsl:if test="COMMISSION_RECEIVED_NUMBER and number(COMMISSION_RECEIVED_NUMBER) != 'NaN' and number(COMMISSION_RECEIVED_NUMBER) != 0">
                      <COMMISSION><NUMBER TYPE="SHORT_PRICE"><xsl:value-of select="COMMISSION_RECEIVED_NUMBER"/></NUMBER></COMMISSION>
                    </xsl:if>

                    <xsl:if test="INTEREST and number(INTEREST) != 'NaN' and number(INTEREST) != 0">
                      <INTEREST><NUMBER TYPE="SHORT_PRICE"><xsl:value-of select="INTEREST"/></NUMBER></INTEREST>
                    </xsl:if>
                    
                    <xsl:if test="SEC_FEE_NUMBER and number(SEC_FEE_NUMBER) != 'NaN' and number(SEC_FEE_NUMBER) != 0">
                      <SEC_FEE><NUMBER TYPE="SHORT_PRICE"><xsl:value-of select="SEC_FEE_NUMBER"/></NUMBER></SEC_FEE>
                    </xsl:if>

                    <xsl:if test="POSTAGE and number(POSTAGE) != 'NaN' and number(POSTAGE) != 0">
                      <POSTAGE><NUMBER TYPE="SHORT_PRICE"><xsl:value-of select="POSTAGE"/></NUMBER></POSTAGE>
                    </xsl:if>

                    <xsl:if test="GOVERNMENT_FEE and number(GOVERNMENT_FEE) != 'NaN' and number(GOVERNMENT_FEE) != 0">
                      <GOVERNMENT_FEE><NUMBER TYPE="SHORT_PRICE"><xsl:value-of select="GOVERNMENT_FEE"/></NUMBER></GOVERNMENT_FEE>
                    </xsl:if>

                    <SUB_TOTAL><NUMBER TYPE="SHORT_PRICE"><xsl:value-of select="NET_AMOUNT * $negate"/></NUMBER></SUB_TOTAL>
                    <xsl:if test="NET_AMOUNT_CURRENCY">
                      <SUB_TOTAL_CCY><xsl:value-of select="NET_AMOUNT_CURRENCY"/></SUB_TOTAL_CCY>
                    </xsl:if>

                    <NET_AMOUNT><NUMBER TYPE="SHORT_PRICE"><xsl:value-of select="NET_AMOUNT * $negate"/></NUMBER></NET_AMOUNT>
                    <xsl:if test="NET_AMOUNT_CURRENCY">
                      <NET_AMOUNT_CCY><xsl:value-of select="NET_AMOUNT_CURRENCY"/></NET_AMOUNT_CCY>
                    </xsl:if>
                </TRANSACTION_CHARGES>
            </TRANSACTION_DETAILS>

            <REFERENCE_DATA>
                <OUR_REF><xsl:value-of select="CONTRACT_REF"/></OUR_REF>
                <ISIN_CUSIP><xsl:value-of select="SECURITY_ISIN"/></ISIN_CUSIP>
                <TICKER_SYMBOL><xsl:value-of select="TICKER_SYMBOL"/></TICKER_SYMBOL>
                <SEC_NO><xsl:value-of select="SECURITY_NUMBER"/></SEC_NO>
                <ACCOUNT_NUM_ABBR>
                  <xsl:value-of select="substring(ACCOUNT_NAME,1,3)"/>
                </ACCOUNT_NUM_ABBR>
                <SUPPRESSION_FLAG>
                    <xsl:choose>
                        <xsl:when test="SUPPRESSION_FLAG = 'NONE'">K</xsl:when>
                        <xsl:otherwise>A</xsl:otherwise>
                    </xsl:choose>
                </SUPPRESSION_FLAG>
            </REFERENCE_DATA>

            <xsl:if test="EOD_NAC_FLAG">
              <xsl:copy-of select="EOD_NAC_FLAG"/>
            </xsl:if>

        </CONFIRMATION>

    </xsl:template>

    <xsl:template match="DATE_TIME">
      <xsl:param    name="allCaps" select="true()"/>

      <xsl:variable name="year" select="substring(RAWDATE,1,4)"/>
      <xsl:variable name="monthNumber" select="substring(RAWDATE,5,2)"/>
      <xsl:variable name="dayNumber" select="substring(RAWDATE,7,2)"/>

      <xsl:variable name="monthNameCapital">
        <xsl:choose>
          <xsl:when test="$monthNumber = '01'">January</xsl:when>
          <xsl:when test="$monthNumber = '02'">February</xsl:when>
          <xsl:when test="$monthNumber = '03'">March</xsl:when>
          <xsl:when test="$monthNumber = '04'">April</xsl:when>
          <xsl:when test="$monthNumber = '05'">May</xsl:when>
          <xsl:when test="$monthNumber = '06'">June</xsl:when>
          <xsl:when test="$monthNumber = '07'">July</xsl:when>
          <xsl:when test="$monthNumber = '08'">August</xsl:when>
          <xsl:when test="$monthNumber = '09'">September</xsl:when>
          <xsl:when test="$monthNumber = '10'">October</xsl:when>
          <xsl:when test="$monthNumber = '11'">November</xsl:when>
          <xsl:when test="$monthNumber = '12'">December</xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">Current date month is not valid.</xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="monthName">
        <xsl:choose>
          <xsl:when test="$allCaps = 'true'">
            <xsl:value-of select="translate($monthNameCapital, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$monthNameCapital"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:value-of select="concat(normalize-space($monthName), ' ', $dayNumber, ', ', $year)"/>
    </xsl:template>

    <xsl:template name="massage-memo-text">
        <xsl:param name="text"/>

        <xsl:choose>
            <xsl:when test="contains($text, 'AVERAGE PRICE')">
                AVG PRICE DETAILS UPON REQUEST
            </xsl:when>

            <xsl:otherwise>
                <xsl:value-of select="$text"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

</xsl:stylesheet>
