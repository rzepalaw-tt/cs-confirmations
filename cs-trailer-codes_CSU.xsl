<?xml version="1.0" encoding="utf-8"?>
        
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:output method="xml" version="1.0" indent="yes"/>

<xsl:variable name="trailer-principal">90</xsl:variable>
<xsl:variable name="trailer-agent">93</xsl:variable>
<xsl:variable name="trailer-ncib">94</xsl:variable>
<xsl:variable name="trailer-short">S</xsl:variable>
<xsl:variable name="trailer-solicited">SC</xsl:variable>
<xsl:variable name="trailer-unsolicited">UN</xsl:variable>
<xsl:variable name="trailer-ifaswhen">W4</xsl:variable>

<xsl:template name="trailer-text">
    <xsl:param name="trailer-code"/>
    <xsl:choose>
        <xsl:when test="$trailer-code = $trailer-principal">WE ACTED ALL/PART AS PRINCIPAL</xsl:when>
        <xsl:when test="$trailer-code = $trailer-agent">WE ACTED AS AGENT</xsl:when>
        <xsl:when test="$trailer-code = $trailer-ncib">NORMAL COURSE ISSUER BID</xsl:when>
        <xsl:when test="$trailer-code = $trailer-short">SHORT</xsl:when>
        <xsl:when test="$trailer-code = $trailer-solicited">SOLICITED</xsl:when>
        <xsl:when test="$trailer-code = $trailer-unsolicited">UNSOLICITED</xsl:when>
        <xsl:when test="$trailer-code = $trailer-ifaswhen">IF, AS AND WHEN ISSUED</xsl:when>
        <xsl:otherwise>
            <xsl:message terminate="yes">No valid trailer code: "<xsl:value-of select="$trailer-code"/>"</xsl:message>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>

