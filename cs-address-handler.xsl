<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [
        <!ENTITY nbsp "&#160;">
        ]>
<xsl:stylesheet version='1.0'
                xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:exsl="http://exslt.org/common"
                xmlns:str="http://exslt.org/strings"
                extension-element-prefixes="str exsl"
>

<xsl:import href="str.split.template.xsl"/>

<xsl:output method="xml" version="1.0" indent="yes"/>

<xsl:variable name="useNewAddressFormat" select="true()"/>
<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

<xsl:template name="parseAddress">
    <xsl:param name="input"/>

    <xsl:variable name="lineSeparator">||||</xsl:variable>
    <xsl:variable name="attrSeparator">####</xsl:variable>

    <POSTAL_ADDRESS_DATA>
        <xsl:variable name="lines">
            <xsl:call-template name="str:split">
                <xsl:with-param name="string" select="$input"/>
                <xsl:with-param name="pattern" select="$lineSeparator"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:for-each select="exsl:node-set($lines)/token">
            <xsl:variable name="lineData">
                <xsl:call-template name="str:split">
                    <xsl:with-param name="string" select="." />
                    <xsl:with-param name="pattern" select="$attrSeparator" />
                </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="lineDataNodeSet" select="exsl:node-set($lineData)"/>

            <POSTAL_ADDRESS_LINE>
                <xsl:attribute name="TYPE">
                    <xsl:value-of select="$lineDataNodeSet/token[1]"/>
                </xsl:attribute>
                <xsl:value-of select="$lineDataNodeSet/token[2]"/>
            </POSTAL_ADDRESS_LINE>
        </xsl:for-each>
    </POSTAL_ADDRESS_DATA>
</xsl:template>

<xsl:template name="formatStatementAddress">
    <xsl:param name="address"/>

    <xsl:variable name="parsedAddressNodes">
        <xsl:call-template name="parseAddress">
            <xsl:with-param name="input" select="$address"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:apply-templates select="exsl:node-set($parsedAddressNodes)//POSTAL_ADDRESS_DATA"/>
</xsl:template>

<xsl:template match="POSTAL_ADDRESS_DATA">
    <xsl:choose>
        <xsl:when test="$useNewAddressFormat = 'true'">
            <xsl:variable name="countryUpper"
                          select="translate(POSTAL_ADDRESS_LINE[@TYPE='Country'], $lowercase, $uppercase)"/>
            <xsl:message>Found country <xsl:value-of select="$countryUpper"/></xsl:message>
            <xsl:choose>
                <xsl:when test="$countryUpper = 'CANADA'">
                    <!-- currently Canadian and US addresses use the same format except the last line.
                         If formats diverge, create different templates -->
                    <xsl:call-template name="addressFormatterCanadaPost"/>
                </xsl:when>
                <xsl:otherwise>
                    <!-- currently Canadian and US addresses use the same format except the last line.
                         If formats diverge, create different templates -->
                    <xsl:call-template name="addressFormatterCanadaPost"/>
                    <!-- non-Canadian addresses need country appended -->
                    <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='Country']">
                        <fo:block>
                            <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='Country']"/>
                        </fo:block>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>

        <xsl:otherwise>
            <xsl:for-each select="POSTAL_ADDRESS_LINE">
                <fo:block>
                    <xsl:value-of select="."/>
                </fo:block>
            </xsl:for-each>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="addressFormatterCanadaPost">
    <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='CompanyName']"> <!-- required field -->
        <fo:block>
            <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='CompanyName']"/>
        </fo:block>
    </xsl:if>

    <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='Department']"> <!-- optional field -->
        <fo:block>
            <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='Department']"/>
        </fo:block>
    </xsl:if>

    <!-- Building optional, StreetAddress1 mandatory -->
    <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='Building'] or
                  POSTAL_ADDRESS_LINE[@TYPE='StreetAddress1']">
        <fo:block>
            <!-- optional field -->
            <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='Building']">
                <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='Building']"/>
            </xsl:if>
            <!-- optional field -->
            <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='Building'] and
                          POSTAL_ADDRESS_LINE[@TYPE='StreetAddress1']">
                <xsl:text>&nbsp;-&nbsp;</xsl:text>
            </xsl:if>
            <!-- required field -->
            <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='StreetAddress1']">
                <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='StreetAddress1']"/>
            </xsl:if>
        </fo:block>
    </xsl:if>

    <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='StreetAddress2']"> <!-- optional field -->
        <fo:block>
            <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='StreetAddress2']"/>
        </fo:block>
    </xsl:if>

    <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='StreetAddress3']"> <!-- optional field -->
        <fo:block>
            <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='StreetAddress3']"/>
        </fo:block>
    </xsl:if>

    <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='TownOrCity'] or
                  POSTAL_ADDRESS_LINE[@TYPE='RegionOrState'] or
                  POSTAL_ADDRESS_LINE[@TYPE='PostalCode']">
        <fo:block>
            <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='TownOrCity']"> <!-- required field -->
                <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='TownOrCity']"/>
            </xsl:if>

            <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='TownOrCity'] and
                          (POSTAL_ADDRESS_LINE[@TYPE='RegionOrState'] or POSTAL_ADDRESS_LINE[@TYPE='PostalCode'])">
                <xsl:text>,&nbsp;</xsl:text>
            </xsl:if>

            <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='RegionOrState']"> <!-- optional field -->
                <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='RegionOrState']"/>
                <xsl:text>&nbsp;</xsl:text>
            </xsl:if>

            <xsl:if test="POSTAL_ADDRESS_LINE[@TYPE='PostalCode']"> <!-- required field -->
                <xsl:value-of select="POSTAL_ADDRESS_LINE[@TYPE='PostalCode']"/>
            </xsl:if>
        </fo:block>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>

